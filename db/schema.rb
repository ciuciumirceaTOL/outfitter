# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_03_22_195725) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "brands", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_brands_on_name", unique: true
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_categories_on_name", unique: true
  end

  create_table "clothing_items", force: :cascade do |t|
    t.string "name"
    t.bigint "user_id", null: false
    t.bigint "brand_id", null: false
    t.bigint "category_id", null: false
    t.bigint "color_id", null: false
    t.decimal "price"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "image_id"
    t.index ["brand_id"], name: "index_clothing_items_on_brand_id"
    t.index ["category_id"], name: "index_clothing_items_on_category_id"
    t.index ["color_id"], name: "index_clothing_items_on_color_id"
    t.index ["name", "user_id"], name: "index_clothing_items_on_name_and_user_id", unique: true
    t.index ["name"], name: "index_clothing_items_on_name", unique: true
    t.index ["user_id"], name: "index_clothing_items_on_user_id"
  end

  create_table "colors", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_colors_on_name", unique: true
  end

  create_table "images", force: :cascade do |t|
    t.string "file_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "outfit_clothing_items", force: :cascade do |t|
    t.bigint "outfit_id", null: false
    t.bigint "clothing_item_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["clothing_item_id"], name: "index_outfit_clothing_items_on_clothing_item_id"
    t.index ["outfit_id", "clothing_item_id"], name: "index_outfit_clothing_items_on_outfit_id_and_clothing_item_id", unique: true
    t.index ["outfit_id"], name: "index_outfit_clothing_items_on_outfit_id"
  end

  create_table "outfits", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "user_id"], name: "index_outfits_on_name_and_user_id", unique: true
    t.index ["user_id"], name: "index_outfits_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "name"
    t.string "phone"
    t.date "birthdate"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "clothing_items", "brands"
  add_foreign_key "clothing_items", "categories"
  add_foreign_key "clothing_items", "colors"
  add_foreign_key "clothing_items", "users"
  add_foreign_key "outfit_clothing_items", "clothing_items"
  add_foreign_key "outfit_clothing_items", "outfits"
  add_foreign_key "outfits", "users"
end
