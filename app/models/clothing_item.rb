class ClothingItem < ApplicationRecord
  belongs_to :user
  belongs_to :brand
  belongs_to :category
  belongs_to :color
  belongs_to :image, optional: true, dependent: :destroy
  has_many :outfit_clothing_items, dependent: :destroy
  has_many :outfits, through: :outfit_clothing_items

  validates_presence_of :name, :price
  validates_uniqueness_of :name, scope: :user_id
  validates_numericality_of :price, greater_than_or_equal_to: 0

  scope :most_recent_first, -> { order(created_at: :desc) }
  scope :alphabetical, -> { order(name: :asc) }

  before_update :delete_old_image

  def self.for_view
    includes(:image, :brand, :color, :category)
  end

  def image_url_or_default
    image.present? ? image.file_name_url : 'no_image.jpg'
  end

  private

  def delete_old_image
    if image_id_was != image_id
      Image.find_by(id: image_id_was).try(:destroy)
    end
  end

end
