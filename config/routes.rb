Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'users/registrations' }
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :users, only: [:show, :edit, :update] do
    resources :clothing_items, only: [:index, :create, :destroy, :edit, :update]
    resources :outfits
    resource :recommendations, only: [:new, :create]
  end

  resources :images, only: [:create, :destroy], defaults: {format: :json}

  root 'users#show'

  # route for User Sign Out
  devise_scope :user do
    get 'sign_out', to: 'devise/sessions#destroy', as: :signout
  end

end
