class AddIndexToOutfitClothingItems < ActiveRecord::Migration[6.0]
  def change
    add_index :outfit_clothing_items, [:outfit_id, :clothing_item_id], unique: true
  end
end
