import Dropzone from "dropzone";
Dropzone.autoDiscover = false;
window.initDropzone = function (modal) {
  var mediaDropzone;
  mediaDropzone = new Dropzone(modal + " #imageDropzone", { 
    url: "/images", 
    autoProcessQueue: true,
    uploadMultiple: false,
    addRemoveLinks: true,
    parallelUploads: 1,
    maxFiles: 1,
    maxFilesize: 3,
    acceptedFiles: '.png, .jpg, .jpeg',
    params: {
      'authenticity_token': $('meta[name="csrf-token"]').attr('content'),
    },
    init: function () {
      this.on("maxfilesexceeded", function (file) {
        this.removeAllFiles();
        this.addFile(file);
      });
      $(modal + " .dz-button").addClass("btn btn-primary");
      $(modal + " .dz-button").append("<i class='material-icons'>attach_file</i>");
    }   
  });
  mediaDropzone.on("removedfile", function () {
    let id = $(modal + " #clothing_item_image_id").val();
    if(id){
      $.ajax({
        url: "/images/" + id,
        type: "DELETE"
      });
    };
  });
  return mediaDropzone.on("success", function (file, image) {
    $(modal + " #clothing_item_image_id").val(image.id);
  });
};