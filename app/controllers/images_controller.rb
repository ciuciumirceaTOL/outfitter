class ImagesController < ApplicationController

  def create
    @image = Image.new(file_name: params[:file])
    if @image.save
      json_response(@image)
    else 
      json_response(@image.errors.full_messages, :unprocessable_entity)
    end
  end

  def destroy
    @image = Image.find_by(id: params[:id])
    @image.try(:destroy)
  end

  private

  def json_response(object, status = :ok)
    render json: object, status: status
  end

end
