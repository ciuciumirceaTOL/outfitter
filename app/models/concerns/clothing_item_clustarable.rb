module ClothingItemClustarable
  # Concern extracts logic to act as a ClothingItems cluster criteria
  # Used in Brand, Category and Color
  extend ActiveSupport::Concern

  included do
    validates_uniqueness_of :name
    scope :alphabetically, -> { order(name: :asc) }
    has_many :clothing_items
  end

  module ClassMethods

    def for_select
      self.alphabetically.map{|item| [item.name, item.id]}
    end

  end


end