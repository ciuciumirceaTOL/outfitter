class RecommendationsSearch
  POINTS_FOR_BRAND_MATCHING = 20
  POINTS_PER_ITEM = 10
  POINTS_PER_USER_MATCH = 10

  # struct for modelling outfit matches
  RankedMatch = Struct.new(:points, :items) do
    def initialize(*args)
      super(*args)
      # the more items an outfit contains, the more points it gets
      self.points += self.items.size * POINTS_PER_ITEM 
    end
  end

  def initialize(options = {})
    @user = User.find_by(id: options[:user_id])
    @clothing_item = ClothingItem.find_by(id: options[:clothing_item_id])
    @users_clothing_items = @user.clothing_items.where.not(id: options[:clothing_item_id]).select(:color_id, :category_id)
  end

  def run
    return [] if @user.blank? || @clothing_item.blank?
    matches = get_sorted_matching_items
    return matches.pluck(:items)
  end

  private

  def get_sorted_matching_items
    ranked_matches = get_ranked_matches
    add_points_for_users_items(ranked_matches)
    ranked_matches.sort_by(&:points).reverse.first(10)
  end

  def add_points_for_users_items(ranked_matches)
    # if the user has clothing items of the same color and category as teh match
    # points are added, as the match is considered better
    category_and_color_of_user = @users_clothing_items.pluck(:category_id, :color_id)
    ranked_matches.each do |rm|
      category_and_color_of_match = rm.items.pluck(:category_id, :color_id)
      matching_items = category_and_color_of_user & category_and_color_of_match
      rm.points += POINTS_PER_USER_MATCH * matching_items.size
    end
  end

  def get_ranked_matches
    outfits_with_brand_matching = get_outfit_matches({brand_id: @clothing_item.brand_id, color_id: @clothing_item.color_id, category_id: @clothing_item.category_id})
    # outfits with matching brands get an extra 20 points
    ranked_matches = outfits_with_brand_matching.map{|outfit| RankedMatch.new(POINTS_FOR_BRAND_MATCHING, outfit.clothing_items.to_a)}

    outfits_no_brand_matching = get_outfit_matches({color_id: @clothing_item.color_id, category_id: @clothing_item.category_id})
    # outfits with matching brands start with 0
    ranked_matches += outfits_no_brand_matching.map{|outfit| RankedMatch.new(0, outfit.clothing_items.to_a)}
    ranked_matches
  end

  def get_outfit_matches(conditions_hash)
    # method receives a conditions hash for clothing_items
    # it searches the outfits of other users to contain items that fit the conditions
    matching_outfits = Outfit.where.not(user_id: @user.id).joins(:clothing_items)
      .where(clothing_items: conditions_hash).limit(10)

    # a where clause of form where(["clothing_items.color_id != ? OR clothing_items.category_it != ?", brand_id, category_id]) is constructed
    where_array = []
    query = conditions_hash.map{|k,v| "clothing_items.#{k} != ?"}.join(" OR ")
    where_array = [query] + conditions_hash.values

    # the outfits are then returned without the matched items in them 
    outfits_with_match_removed = Outfit.includes(clothing_items: [:image, :brand, :color, :category]).where(id: matching_outfits.pluck(:id)).
      where(where_array).references(:clothing_items)
    return outfits_with_match_removed
  end
end