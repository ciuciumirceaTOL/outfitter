class OutfitsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_user
  before_action :load_outfit, only: [:destroy, :edit, :update, :show]
  before_action :ensure_limit, only: [:new, :create]

  def index
    @outfits = @user.outfits.includes(clothing_items: [:image]).most_recent_first
    @clothes_number = @user.clothing_items.size
  end

  def show
    @clothing_items = @outfit.clothing_items.for_view.alphabetical
  end

  def new
    @outfit = Outfit.new
    @outfit.outfit_clothing_items.build
    @current_clothing_items = []
    @remaining_clothing_items = @user.clothing_items.for_view.most_recent_first
  end

  def create
    @outfit = @user.outfits.create(permitted_params)
    if @outfit.errors.any?
      redirect_back(fallback_location: new_user_outfit_path(@user))
    else
      redirect_to user_outfits_path(@user)
    end
  end

  def destroy
    @outfit.destroy
    @clothes_number = @user.clothing_items.size
  end

  def edit
    @current_clothing_items = @outfit.clothing_items.for_view.alphabetical
    @remaining_clothing_items = @user.clothing_items.for_view.most_recent_first.where.not(id: @current_clothing_items.pluck(:id))
  end

  def update
    if @outfit.update(permitted_params)
      redirect_to user_outfits_path(@user)
    else 
      redirect_back(fallback_location: edit_user_outfit_path(@user, @outfit))
    end
  end

  private

  def load_user
    @user = current_user
  end
  
  def load_outfit
    @outfit = @user.outfits.find(params[:id])
  end

  def ensure_limit
    redirect_back(fallback_location: user_outfits_path(@user)) if @user.outfits.size >= User::MAX_OUTFITS || @user.clothing_items.size < User::MIN_CLOTHES_FOR_OUTFIT
  end

  def permitted_params
    params.require(:outfit).permit(:name, outfit_clothing_items_attributes: [:id, :clothing_item_id, :_destroy])
  end

end
