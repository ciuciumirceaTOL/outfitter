class ClothingItemsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_user 
  before_action :load_clothing_item, only: [:destroy, :edit, :update]

  def index
    @clothing_item = ClothingItem.new
    @clothing_items = @user.clothing_items.for_view.most_recent_first
  end

  def create
    @clothing_item = @user.clothing_items.create(permitted_params)
    if @clothing_item.errors.any?
      remove_image
    else
      @updated_clothing_item = @clothing_item
      @clothing_item = ClothingItem.new
    end
  end

  def destroy
    @clothing_item.destroy
  end

  def edit
  end

  def update
    @success = @clothing_item.update(permitted_params)
  end

  private

  def load_user
    @user = current_user
  end

  def load_clothing_item
    @clothing_item = @user.clothing_items.find(params[:id])
  end

  def permitted_params
    params.require(:clothing_item).permit(:name, :price, :brand_id, :category_id, :color_id, :image_id)
  end

  def remove_image
    @clothing_item.image.try(:destroy)
  end

end
