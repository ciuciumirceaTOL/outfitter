class RecommendationsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_user

  def new
    @outfits_size = @user.outfits.size
    @clothing_items = @user.clothing_items
  end

  def create
    @recommendations = RecommendationsSearch.new({
      user_id: @user.id,
      clothing_item_id: params[:clothing_item_id]
    }).run
  end

  private

  def load_user
    @user = current_user
  end
  
end
