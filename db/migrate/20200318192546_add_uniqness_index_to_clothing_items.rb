class AddUniqnessIndexToClothingItems < ActiveRecord::Migration[6.0]
  def change
    add_index :clothing_items, [:name, :user_id], unique: true
  end
end
