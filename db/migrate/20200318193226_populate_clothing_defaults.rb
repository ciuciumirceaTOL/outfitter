class PopulateClothingDefaults < ActiveRecord::Migration[6.0]
  def up
    brands_array = ["Nike", "H&M", "Zara", "Adidas", "Louis Vuitton", "Cartier", "Hermes", "Gucci", "Rolex",
      "Victoria's Secret", "Burberry", "Polo Ralph Lauren", "Under Armour", "Armani", "Puma", "Ray-Ban",
      "Michael Kors", "Tommy Hilfiger", "Calvin Klein"]
    brands_hashes = brands_array.map{|brand_name| {name: brand_name, created_at: Time.now, updated_at: Time.now} }
    Brand.insert_all(brands_hashes)

    categories_array = ["Jackets", "Coats", "Trousers", "Jeans", "Shorts", "Underwear", "Suits", "Skirts",
      "Dresses", "Shoes", "Sweaters", "Accessories", "T-shirts", "Shirts"]
    categories_array = categories_array.map{|category_name| {name: category_name, created_at: Time.now, updated_at: Time.now} }
    Category.insert_all(categories_array)

    colors_array = ["Red", "Purple", "Pink", "Blue", "Green", "Yellow", "Black", "White", "Orange", "Gray"]
    colors_array = colors_array.map{|color_name| {name: color_name, created_at: Time.now, updated_at: Time.now} }
    Color.insert_all(colors_array)
  end

  def down
    Brand.destroy_all
    Category.destroy_all
    Color.destroy_all
  end
end
