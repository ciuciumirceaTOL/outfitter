class OutfitClothingItem < ApplicationRecord
  belongs_to :outfit
  belongs_to :clothing_item

  validates_uniqueness_of :clothing_item_id, scope: :outfit_id
end
