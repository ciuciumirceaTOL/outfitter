class AddImageIdToClothingItems < ActiveRecord::Migration[6.0]
  def change
    add_column :clothing_items, :image_id, :integer
  end
end
