module LayoutsHelper
  def navbar_active_class(menu)
    controller = params[:controller]
    
    active = case menu
    when "home"
      controller == "users"
    when "clothes"
      controller == "clothing_items"
    when "outfits"
      controller == "outfits"
    when "recommendations"
      controller == "recommendations"
    else
      false
    end

    active ? "active" : ""
  end
end