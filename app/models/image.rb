class Image < ApplicationRecord
  validates_presence_of :file_name
  mount_uploader :file_name, ImageUploader
end
