class User < ApplicationRecord
  MAX_OUTFITS = 10
  MIN_CLOTHES_FOR_OUTFIT = 3
  MIN_OUTFITS_FOR_RECOMMENDATION = 1
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  
  validates_presence_of :name, :phone, :birthdate
  validates_uniqueness_of :phone
  has_many :clothing_items
  has_many :outfits

end
