import dragula from "dragula";

window.initDragula = function (){
  let isTouch = ('ontouchstart' in document.documentElement);

  if(isTouch){
    $(".clothing_item_card").on("touchstart", function (e) {
      this.notSwipe = true;
    });
    $(".clothing_item_card").on("touchmove", function (e) {
      this.notSwipe = false;
    });
    $(".clothing_item_card").on("touchend", function (e) {
      if (this.notSwipe){
        var to_move = $(this).parent();
        let id = to_move.attr('data-id');
        if($(this).closest("#right").length){
          $("#left").prepend(to_move.clone(true));
          to_move.remove();
          addField(id);
          updateRoom(false);
        }
        else{
          $("#right").prepend(to_move.clone(true));
          to_move.remove();
          removeField(id);
          updateRoom(true);
        }
        computeTotalAmount();
      }
    });
  }
  else{
    var drake = dragula([document.getElementById('left'), document.getElementById('right')],
      {
        accepts: function (el, target, source, sibling) {
          return target.id != 'left' || $('#left .clothing_item_card').length < 6;
        }
      }
    );

    drake.on('drop', function (el, target, source, sibling) {
      computeTotalAmount();
      let id = el.getAttribute('data-id');
      if (target.id === "left") {
        addField(id);
        updateRoom(false);
      }
      else {
        removeField(id);
        updateRoom(true);
      };
    });
  }

  if ($(".destroy_item_").length) {
    $("#outfit_outfit_clothing_items_attributes_0_clothing_item_id").remove();
    $(".destroy_item_").remove();
  }
};

function addField(id){
  if($(".destroy_item_" + id).length){
    $(".destroy_item_" + id).prop("checked", false);
  }
  else{
    let size = $('.clothing_item_id_field').length;
    let field = '<input type="hidden" name="outfit[outfit_clothing_items_attributes][' + size + '][clothing_item_id]" class="clothing_item_id_field" id="outfit_outfit_clothing_items_attributes_' + size + '_clothing_item_id" value="'+ id + '"></input>';
    $(".outfit_clothing_items_fields").append(field);
  }
};

function removeField(id) {
  if($(".destroy_item_" + id).length){
    $(".destroy_item_" + id).prop("checked", true);
  }
  else{
    $('.outfit_clothing_items_fields :input[value="' + id + '"]').remove();
  }
};

function updateRoom(increment){
  var room = parseInt($(".remaining_room").html());
  room = increment ? room + 1 : room - 1;
  $(".remaining_room").html(room);
};

function computeTotalAmount(){
  var sum = 0;
  $('#left .clothing_item_card').each(function () {
    sum += parseFloat($(this).data().price);
  });
  $(".total_cost").html('$' + sum.toFixed(2));
};