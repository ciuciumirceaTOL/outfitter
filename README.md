# README
Repository for the [Outfitter](https://outfitter-application.herokuapp.com/) Application. 


## Resources used:
### Technologies, Frameworks, Libraries
#### Backend: 
*  **Ruby 2.7.0**
*  **Rails 6.0.2.1**
#### Frontend
*  **Bootstrap 4**
*  **[Material UI Theme](https://demos.creative-tim.com/material-kit/index.html)**
*  **[Dropzone.js](https://www.dropzonejs.com/#)**
*  **[Dragula](https://bevacqua.github.io/dragula/)**
### Non-Tech Resources
*  **[Images](https://unsplash.com/) Source**


## Prerequisites
1.  [Ruby 2.7.0](https://www.ruby-lang.org/en/documentation/installation/) installed
2.  A database (preferably [PostgreSQL](https://www.postgresql.org/download/)) installed 
3.  [Yarn](https://classic.yarnpkg.com/en/docs/install/#mac-stable) installed
4.  [AWS S3](https://aws.amazon.com/s3/) set up


## Steps to install: 

1.  Clone project: 
`$ git clone https://gitlab.com/ciuciumirceaTOL/outfitter.git`

2.  Install bundler gem if not already installed: `$ gem install bundler`

3.  Install gems:
`$ bundle install`

4.  Install yarn dependencies:
`$ yarn install`

5. The **database.yml** file is setup for PostgreSQL, both in development and on production.<br/> 
If another database is used, the file should be [updated](https://edgeguides.rubyonrails.org/configuring.html#configuring-a-database). 
6. The database default name for development is **outfitter_development**. <br/>
Create database: `$ createdb outfitter_development`
7. Run migrations: `$ rails db:migrate`
8. Set up AWS S3 credentials. All images added to the application are stored in S3.<br/> 
To setup create a **.env** file in the root of the project. Add the credentials inside the file. <br/>
`S3_BUCKET={bucket-name}` <br/>
`ACCESS_KEY={aws-access-key}` <br/>
`SECRET_KEY={aws-secret-key}` <br/>
More detailed [instructions](https://github.com/bkeepers/dotenv#usage). 
9. Start server: `$ rails s`


