import Rails from "@rails/ujs"
window.outfitter = window.outfitter or {}
((recommendations, $) ->

  recommendations.selectClothingItem = ->
    $(document).off "click", ".clothing_options .clothing_item_card"
    $(document).on "click", ".clothing_options .clothing_item_card", ->
      if !$(@).hasClass(".selected_clothing_item")
        $('.recommendations_area .row').empty()
        $(".spinner-border").show()
        $(".clothing_options .clothing_item_card").removeClass("selected_clothing_item")
        $(@).addClass("selected_clothing_item")
        id = $(@).parent().data().id
        $("#clothing_item_id").val(id)
        form = document.querySelector('.recommendations_form')
        Rails.fire(form, 'submit')
      

) window.outfitter.recommendations = window.outfitter.recommendations or {}, jQuery
