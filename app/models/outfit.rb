class Outfit < ApplicationRecord
  MAX_ROOM = 6
  belongs_to :user

  validates_presence_of :name

  has_many :outfit_clothing_items, dependent: :destroy
  has_many :clothing_items, through: :outfit_clothing_items

  accepts_nested_attributes_for :outfit_clothing_items, allow_destroy: true

  scope :most_recent_first, -> { order(created_at: :desc) }

  def remaining_room
    @remaining_room ||= MAX_ROOM - size
  end

  def price
    @price ||= clothing_items.pluck(:price).sum
  end

  def size
    @size ||= clothing_items.size
  end
end
