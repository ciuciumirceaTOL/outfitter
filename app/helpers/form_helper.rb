module FormHelper
  def add_error_label(form, field)
    content_tag(:label, form.object.errors[field].try(:first), class: 'control-label bmd-label-static')
  end

  def form_group_for(form, field, opts={}, &block)
    has_errors = form.object.errors[field].present?

    content_tag :div, class: "form-group label-floating #{'has-danger' if has_errors}" do
      concat add_error_label(form, field) if has_errors
      concat capture(&block)
      concat content_tag(:span, "clear", class: 'material-icons form-control-feedback') if has_errors
    end
  end

  def disable_with_spinner(text)
    "<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> #{text}.."
  end
end