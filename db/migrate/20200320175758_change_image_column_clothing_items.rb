class ChangeImageColumnClothingItems < ActiveRecord::Migration[6.0]
  def change
    remove_column :clothing_items, :image
  end
end
