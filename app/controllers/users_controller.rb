class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :load_user

  def show 
    @clothing_items_size = @user.clothing_items.size
    @outfits_size = @user.outfits.size
  end

  private

  def load_user
    @user = current_user
  end

end
